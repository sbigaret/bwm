import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.DoubleStream;

import org.junit.Assert;
import org.junit.Test;

import pl.poznan.put.bwm.algorithm.Solver;

public class SolverTest {

	@Test
	public void test() {
		double[] weights = {0.05459617, 0.46829378, 0.04877501, 0.1123578 , 0.31597723};
		List<Double> best_to_others = Arrays.asList(8.57741024, 1.        , 9.60110055, 4.16787951, 1.48204913);
		List<Double> others_to_worst = Arrays.asList(1.11934725, 9.60110055, 1.        , 2.30359359, 6.47826063);
		int best_id = 1;
		int worst_id = 2;
		int size = 5;		
		List<Double> weights_calc = solve(size, best_to_others, others_to_worst,best_id,worst_id);

		Assert.assertEquals(1.0, weights_calc.stream().mapToDouble(d -> d).sum(),0.01);

		double[] weights_calc2 = weights_calc.stream().mapToDouble(d -> d).toArray();
		Assert.assertArrayEquals(weights_calc2, weights, 0.001);
		
	}
	public List<Double> solve(int size, List<Double> best_to_others, List<Double> others_to_worst, int best_id, int worst_id) {
		Solver solver = new Solver(size);
		solver.run(best_to_others, others_to_worst, best_id, worst_id);
		List<Double> weights = solver.getResult();
		Double optim_value = solver.getOptymalizationValue();
		System.out.println("optim_value: "+optim_value);
		solver.delete();
		return weights;
	}

}
