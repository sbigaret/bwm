import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.xmcda.ProgramExecutionResult;


import pl.poznan.put.bwm.xmcda.Utils;

import pl.poznan.put.bwm.xmcda.WeightsXMCDA;


public class XMCDATest_base {
	protected String inputDirectory;
	protected String outputDirectory;
	protected Utils.XMCDA_VERSION version;

	private String[] alternatives_ids;
	
    public XMCDATest_base(String inputDirectory, String outputDirectory,Utils.XMCDA_VERSION version ) {
		this.inputDirectory = inputDirectory;
		this.outputDirectory = outputDirectory;
		this.version = version;

    }
    
    protected boolean Test() {
    	System.out.println(inputDirectory);
		
		final File prgExecResultsFile = new File(outputDirectory, "messages.xml");

		final ProgramExecutionResult executionResult = new ProgramExecutionResult();
		
		WeightsXMCDA.run(prgExecResultsFile, executionResult, inputDirectory, outputDirectory, version);

		try {
			Utils.writeProgramExecutionResults(prgExecResultsFile, executionResult, version);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			
			return false;			
		}
		return executionResult.isOk();
    }
    
    
    protected static Collection<Object[]> getData(String test_folder,Utils.XMCDA_VERSION version){
    	List<String> folders_in  =  ListFolders(test_folder,"in");
    	List<String> folders_out =  ListFolders(test_folder,"out");
    	
    	Object[][] combined = new Object[folders_in.size()][];
    	for(int i =0;i<folders_in.size();i++)
    	{
    		combined[i] = new Object[3];
    		combined[i][0] = folders_in.get(i);
    		combined[i][1] = folders_out.get(i);
    		combined[i][2] = version;
    	}

        return Arrays.asList(combined);
    }
    protected static List<String> ListFolders(String folder,String endsWith){
		try (Stream<Path> walk = Files.walk(Paths.get(folder))) {

			List<String> result = walk.filter(Files::isDirectory)
					.map(x -> x.toString()).filter(f -> f.endsWith(endsWith)).collect(Collectors.toList());

			return result;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
    
    
    
    
    protected HashMap<String,HashMap<String,Double>> GetDataTestFile(String inputDirectory,String algorithm) {
    	String file = inputDirectory.split("\\\\")[2].replace("pair_", "").replace(algorithm+"_in", "test_data.csv");
    	return ReadCSV(file);
    }
    	
    protected void IsEqualFlow(Map<String, Double> f1 ,Map<String, Double> f2) {
    	assertEquals(f1.size(),f2.size());
    	for(int i =0;i<f1.size();i++) {
    		String key = alternatives_ids[i+1];
    		assertEquals(f1.get(key), f2.get(key),0.001);
    	}
    }
    
    protected HashMap<String,HashMap<String,Double>> ReadCSV(String file) {
		HashMap<String,HashMap<String,Double>> values = null;
		BufferedReader csvReader;
		try {
			csvReader = new BufferedReader(new FileReader(file));
			String row;
			alternatives_ids = null;
			values = new HashMap<String,HashMap<String,Double>>();
			boolean headers = true;
			while ((row = csvReader.readLine()) != null) {
			    String[] data = row.split(",");
			    if(headers) {
			    	headers = false;
			    	alternatives_ids = data;
			    }
			    else
			    {
			    	HashMap<String,Double> metric = new HashMap<String,Double>();
			    	for(int i =1;i<alternatives_ids.length;i++) {
			    		double value = Double.parseDouble(data[i]);
			    		metric.put(alternatives_ids[i], value);			    		
			    	}
			    	values.put(data[0], metric);
			    }

			}
			csvReader.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return values;
	}
}
