package pl.poznan.put.bwm.xmcda;

import java.util.Set;

import org.xmcda.CriteriaMatrix;
import org.xmcda.Criterion;
import org.xmcda.XMCDA;
import org.xmcda.utils.Coord;

import pl.poznan.put.bwm.xmcda.InputsHandler.InputType;

public class CriteriaValues {
	   private CriteriaMatrix criteriaValues;
	   private InputType inputType;
	   public CriteriaValues(CriteriaMatrix criteriaValues, InputType inputType) {
		   this.criteriaValues = criteriaValues;
		   this.inputType = inputType;
		   
	   }
	   public Set<Coord<Criterion, Criterion>> keySet() {
		   if (inputType == InputType.INTEGER) {
			   return ((CriteriaMatrix<Integer>) criteriaValues).keySet();
		   }
		   else
		   {
			   return ((CriteriaMatrix<Double>) criteriaValues).keySet();
		   }
	   }
	   public Double getValue(Coord<Criterion, Criterion> coord) {
		   if (inputType == InputType.INTEGER) {
			   return ((CriteriaMatrix<Integer>) criteriaValues).get(coord).get(0).getValue().intValue() +0.0;
		   }
		   else
		   {
			   return ((CriteriaMatrix<Double>) criteriaValues).get(coord).get(0).getValue().doubleValue();
		   }
	   }
}