/**
 *
 */
package pl.poznan.put.bwm.xmcda;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

import org.xmcda.ProgramExecutionResult;
import org.xmcda.XMCDA;

import pl.poznan.put.bwm.xmcda.InputsHandler.Inputs;
import pl.poznan.put.bwm.xmcda.Utils.InvalidCommandLineException;
import pl.poznan.put.bwm.algorithm.Bwm;
import pl.poznan.put.bwm.algorithm.ResultContainer;
import pl.poznan.put.bwm.xmcda.InputFile;
import pl.poznan.put.bwm.xmcda.Utils;

/**
 * 
 */
public class WeightsXMCDA {
	/**
	 * 
	 * 
	 * @param args
	 * @throws InvalidCommandLineException 
	 */
	public static void main(String[] args) throws InvalidCommandLineException {
		final Utils.XMCDA_VERSION version = readVersion(args);
		final Utils.Arguments params = readParams(args);

		final String inputDirectory = params.inputDirectory;
		final String outputDirectory = params.outputDirectory;
		
		final File prgExecResultsFile = new File(outputDirectory, "messages.xml");

		final ProgramExecutionResult executionResult = new ProgramExecutionResult();
		System.out.println(inputDirectory);
		
		run(prgExecResultsFile, executionResult, inputDirectory, outputDirectory, version);
		

		exitProgram(executionResult, prgExecResultsFile, version);
	}
	public static void run(File prgExecResultsFile, ProgramExecutionResult executionResult, String inputDirectory, String outputDirectory, Utils.XMCDA_VERSION version) {
		Map<String, InputFile> files = initFiles();		
		final XMCDA xmcda = InputFileLoader.loadFiles(files, inputDirectory, executionResult, prgExecResultsFile,
				version);
		if (!ErrorChecker.checkErrors(executionResult, xmcda))
			return;

		final InputsHandler.Inputs inputs = InputsHandler.checkAndExtractInputs(xmcda, executionResult);
		if (!ErrorChecker.checkErrors(executionResult, inputs))
			return;

		final  Map<String, Double> weights = calcWeights(inputs, executionResult);
		if (!ErrorChecker.checkErrors(executionResult, weights))
			return;
		
		final Map<String, XMCDA> xmcdaResults = OutputsHandler.convert(weights, inputs.criteria_ids, executionResult);

		OutputFileWriter.writeResultFiles(xmcdaResults, executionResult, outputDirectory, version);
	}
	
	
	private static Map<String, Double> calcWeights(Inputs inputs, ProgramExecutionResult executionResult) {	
		ResultContainer  resultContainer = Bwm.calcWeights(inputs);
		if (!resultContainer.isFound_solution()) {
			executionResult.addWarning("The optimal linear programing solution not found.");
		}
		
		executionResult.addInfo("The optimal value of ksi found by linear programing is " + resultContainer.getKsi());
		executionResult.addInfo("The consistency ratio of result is " + resultContainer.getConsistencyRatio());

		return resultContainer.getWeights();
	}



	private static Utils.Arguments readParams(String[] args) {
		Utils.Arguments params = null;
		ArrayList<String> argsList = new ArrayList<String>(Arrays.asList(args));
		argsList.remove("--v2");
		argsList.remove("--v3");
		try {
			params = Utils.parseCmdLineArguments((String[]) argsList.toArray(new String[] {}));
		} catch (InvalidCommandLineException e) {
			System.err.println("Missing mandatory options. Required: [--v2|--v3] -i input_dir -o output_dir");
			System.exit(-1);
		}
		return params;
	}

	private static Utils.XMCDA_VERSION readVersion(String[] args) {
		Utils.XMCDA_VERSION version = Utils.XMCDA_VERSION.v2;
		;
		final ArrayList<String> argsList = new ArrayList<String>(Arrays.asList(args));
		if (argsList.remove("--v2")) {
			version = Utils.XMCDA_VERSION.v2;
		} else if (argsList.remove("--v3")) {
			version = Utils.XMCDA_VERSION.v3;
		} else {
			System.err.println("Missing mandatory option --v2 or --v3");
			System.exit(-1);
		}
		return version;
	}

	private static Map<String, InputFile> initFiles() {
		Map<String, InputFile> files = new LinkedHashMap<>();
		files.put("methodParameters",
				new InputFile("methodParameters", "programParameters", "parameters.xml", true));
		files.put("critera", new InputFile("criteria", "criteria", "criteria.xml", true));		
		files.put("best_to_others",
				new InputFile("criteriaComparisons", "criteriaMatrix", "best_to_others.xml", true));
		files.put("others_to_worst",
				new InputFile("criteriaComparisons", "criteriaMatrix", "others_to_worst.xml", true));
		return files;
	}

	private static void exitProgram(ProgramExecutionResult executionResult, File prgExecResultsFile,
			Utils.XMCDA_VERSION version) {
		Utils.writeProgramExecutionResultsAndExit(prgExecResultsFile, executionResult, version);
	}


	

}
