package pl.poznan.put.bwm.xmcda;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.xmcda.CriteriaMatrix;
import org.xmcda.Criterion;
import org.xmcda.ProgramExecutionResult;
import org.xmcda.ProgramParameter;
import org.xmcda.QualifiedValue;
import org.xmcda.XMCDA;
import org.xmcda.utils.Coord;

/**
 * 
 */
public class InputsHandler {	
	public enum InputType {
		REAL("real"), INTEGER("integer");

		private String label;

		private InputType(String paramLabel) {
			label = paramLabel;
		}

		/**
		 * Return the label for this AlgorithmType Parameter
		 *
		 * @return the parameter's label
		 */
		public final String getLabel() {
			return label;
		}

		/**
		 * Returns the parameter's label
		 *
		 * @return the parameter's label
		 */
		@Override
		public String toString() {
			return label;
		}

		/**
		 * Returns the {@link AlgorithmType} with the specified label. It
		 * behaves like {@link #valueOf(String)} with the exception
		 *
		 * @param parameterLabel
		 *            the label of the constant to return
		 * @return the enum constant with the specified label
		 * @throws IllegalArgumentException
		 *             if there is no AlgorithmType with this label
		 * @throws NullPointerException
		 *             if parameterLabel is null
		 */
		public static InputType fromString(String parameterLabel) {
			if (parameterLabel == null)
				throw new NullPointerException("parameterLabel is null");
			for (InputType op : InputType.values()) {
				if (op.toString().equals(parameterLabel))
					return op;
			}
			throw new IllegalArgumentException("Enum InputType with label " + parameterLabel + " not found");
		}
	}
	public static class Inputs {

		public List<String> criteria_ids;
		public  Map<String, Double> best_to_others;
		public  Map<String, Double> others_to_worst;
		public String best;
		public String worst;
		public InputType inputType;
	}

	/**
	 *
	 * @param xmcda
	 * @param xmcda_exec_results
	 * @return
	 */
	static public Inputs checkAndExtractInputs(XMCDA xmcda, ProgramExecutionResult xmcda_exec_results) {
		Inputs inputsDict = checkInputs(xmcda, xmcda_exec_results);

		if (xmcda_exec_results.isError())
			return null;

		return extractInputs(inputsDict, xmcda, xmcda_exec_results);
	}

	/**
	 * @param xmcda
	 * @param errors
	 * @return Inputs
	 */
    protected static Inputs checkInputs(XMCDA xmcda, ProgramExecutionResult errors){
        Inputs inputs = new Inputs();
        checkCriteria(xmcda, errors);
        checkCriteriaValues(xmcda, errors);
        checkParameters(inputs,xmcda,errors);
        return inputs;
    }

    private static void checkCriteria(XMCDA xmcda, ProgramExecutionResult errors) {

        if (xmcda.criteria.size() == 0) {
            errors.addError("No criteria found");
            return;
        }
        if (xmcda.criteria.getActiveCriteria().size() == 0) {
            errors.addError("No active criteria found");
            return;
        }
    }

    private static void checkCriteriaValues(XMCDA xmcda, ProgramExecutionResult errors) {

        if(xmcda.criteriaMatricesList.size() == 0){
            errors.addError("No criteria comparisons found");
            return;
        }
    }
    private static void checkParameters(Inputs inputs, XMCDA xmcda, ProgramExecutionResult errors) {
    	InputType inputType = null;


		if (xmcda.programParametersList.size() > 1) {
			errors.addError("Only one list of parameters is expected");
			return;
		}
		if (xmcda.programParametersList.size() == 0) {
			errors.addError("List of parameters not found");
			return;
		}
		if (xmcda.programParametersList.get(0).size() != 1) {
			errors.addError("Exactly one parameter are expected");
			return;
		}

		//final ProgramParameter<?> prgParam = xmcda.programParametersList.get(0).get(0);

		for(ProgramParameter<?> prgParam : xmcda.programParametersList.get(0))
		{
			if ("input_type".equals(prgParam.name())) {
				
				if (prgParam.getValues() == null || (prgParam.getValues() != null && prgParam.getValues().size() != 1)) {
					errors.addError("input_type parameter must have a single (label) value only");
					return;
				}
				try {
					final String parameterValue = (String) prgParam.getValues().get(0).getValue();
					inputType = InputType.fromString((String) parameterValue);
				} catch (Throwable throwable) {
					StringBuffer valid_values = new StringBuffer();
					for (InputType op : InputType.values()) {
						valid_values.append(op.getLabel()).append(", ");
					}
					String err = "Invalid value for algorithm_type parameter, it must be a label, ";
					err += "possible values are: " + valid_values.substring(0, valid_values.length() - 2);
					errors.addError(err);
					inputType = null;
				}
				inputs.inputType = inputType;
				
			}
		}
    }

    /**
    *
    * @param inputs
    * @param xmcda
    * @param xmcda_execution_results
    * @return
    */
   protected static Inputs extractInputs(Inputs inputs, XMCDA xmcda, ProgramExecutionResult xmcda_execution_results) {

       extractCriteria(inputs, xmcda, xmcda_execution_results);
       extractBestToOthers(inputs, xmcda, xmcda_execution_results);
       extractOthersToWorst(inputs, xmcda, xmcda_execution_results);
       checkPreferences(inputs, xmcda, xmcda_execution_results);
       return inputs;
   }

   private static void checkPreferences(Inputs inputs, XMCDA xmcda, ProgramExecutionResult errors) {
	   if(Double.compare(inputs.best_to_others.get(inputs.worst), inputs.others_to_worst.get(inputs.best))!=0) {
		   errors.addError("The preference of the best criterion over the worst should be equal in both files best_to_others and others_to_worst.");
	   }
   }
   private static void extractCriteria(Inputs inputs, XMCDA xmcda, ProgramExecutionResult errors) {

       List<String> criteria_ids = new ArrayList<>();
       for (Criterion criterion : xmcda.criteria) {
           if (criterion.isActive()) {
        	   criteria_ids.add(criterion.id());
           }
       }

       if (criteria_ids.isEmpty()) {
           errors.addError("The criteria list can not be empty.");
       }

       inputs.criteria_ids = criteria_ids;
   }

   
	private static void extractBestToOthers(Inputs inputs, XMCDA xmcda, ProgramExecutionResult errors) {
		CriteriaValues criteriaValues = new CriteriaValues(xmcda.criteriaMatricesList.get(0) , inputs.inputType);
		
		inputs.best_to_others = new LinkedHashMap<String, Double>();
		
		int i = 0;
		for (Coord<Criterion, Criterion> coord : criteriaValues.keySet()) {
			String x = coord.x.id();
			if (i == 0)
			{
				inputs.best = x;
			}
			if (inputs.best != x) {
				errors.addError("Ambiguous values of best criterion: " + inputs.best + " and "+ x);
				return;
			}
			
			String y = coord.y.id();	
			Double value = criteriaValues.getValue(coord);

			if(y.equals(x) && value !=1) {
				errors.addError("The preference of the best criterion ("+inputs.best+") for yourself should be equal to 1.");
				return;
			}
			if (value<1) {
				errors.addError("The preference value should be greater than or equal to 1.");
				return;
			}
			inputs.best_to_others.put(y, value);
			i+=1;
		}
		for (String criterion : inputs.criteria_ids) {
			if (!inputs.best_to_others.containsKey(criterion)) {
				errors.addError("In best_to_others doesn't exist preference to criterion: " + criterion);
				return;
			}
		}
	}
	private static void extractOthersToWorst(Inputs inputs, XMCDA xmcda, ProgramExecutionResult errors) {
		CriteriaValues criteriaValues = new CriteriaValues(xmcda.criteriaMatricesList.get(1), inputs.inputType);
		inputs.others_to_worst = new LinkedHashMap<String, Double>();
		
		int iterarion =0;
		for (Coord<Criterion, Criterion> coord : criteriaValues.keySet()) {
			String y = coord.y.id();
			if (iterarion == 0)
			{
				inputs.worst = y;
			}
			if (inputs.worst != y) {
				errors.addError("Ambiguous values of worst criterion: " + inputs.worst + " and "+ y);
				return;
			}
			String x = coord.x.id();
			Double value = criteriaValues.getValue(coord);
			if(y.equals(x) && value !=1) {
				errors.addError("The preference of the worst criterion ("+inputs.worst+") for yourself should be equal to 1.");
				return;
			}
			if (value<1) {
				errors.addError("The preference value should be greater than or equal to 1.");
				return;
			}
			inputs.others_to_worst.put(x, value);
			iterarion+=1;
		}
		for (String criterion : inputs.criteria_ids) {
			if (!inputs.others_to_worst.containsKey(criterion)) {
				errors.addError("In others_to_worst doesn't exist preference to criterion: " + criterion);
				return;
			}
		}
	}


}