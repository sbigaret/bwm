package pl.poznan.put.bwm.algorithm;

import java.util.LinkedList;
import java.util.List;

public class Logger {

	private boolean print;
	private List<String> logs;
	
	
	public Logger(boolean print) {
		this.print = print;
		this.logs = new LinkedList<String>();
	}
	public void add(String log) {
		this.logs.add(log);
		if(this.print) {
			System.out.println(log);
		}

	}
}
