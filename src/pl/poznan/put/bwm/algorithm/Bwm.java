package pl.poznan.put.bwm.algorithm;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import pl.poznan.put.bwm.xmcda.InputsHandler.Inputs;

public class Bwm {
	
	public static ResultContainer calcWeights(Inputs input) {
		List<Double> best_to_others = translateInput(input.best_to_others, input.criteria_ids);
		List<Double> others_to_worst = translateInput(input.others_to_worst, input.criteria_ids);
		int best_id = translateIds(input.best, input.criteria_ids);
		int worst_id = translateIds(input.worst, input.criteria_ids);
		Solver solver = new Solver(input.criteria_ids.size());
		boolean found_solution = solver.run(best_to_others, others_to_worst, best_id, worst_id);
		List<Double> weights = solver.getResult();
		double ksi = solver.getOptymalizationValue();
		solver.delete();
		
		double consistencyRatio = getConsistencyRatio(input.best_to_others.get(input.worst),ksi);
		
		return new ResultContainer(translateResult(weights,input.criteria_ids), found_solution, ksi, consistencyRatio);
	}
	
	private static List<Double> translateInput(Map<String, Double> weights, List<String> criteria_ids){
		List<Double> result = new ArrayList<Double>();
		for(String ids : criteria_ids) {
			result.add(weights.get(ids));
		}
		return result;		
	}
	
	private static Integer translateIds(String ids, List<String> criteria_ids){
		return 	criteria_ids.indexOf(ids);
	}
	private static Map<String, Double> translateResult(List<Double> weights, List<String> criteria_ids){
		Map<String, Double> result = new LinkedHashMap<String, Double>();
		for(int i=0;i<criteria_ids.size();i++) {
			result.put(criteria_ids.get(i), weights.get(i));
		}
		return result;
	}
	private static double getConsistencyIndex(double abw) {
	    double b = - (1+2*abw);
	    double c = (abw*abw-abw);
	    return (-b-Math.sqrt(b*b-4*c))/(2);
	}
	private static double getConsistencyRatio(double abw, double ksi) {
		if(abw==0 && ksi>0) {
			return Double.POSITIVE_INFINITY;
		}
		else if(abw==0 && ksi ==0) {
			return 0;
		}
		else {
			return ksi/getConsistencyIndex(abw);
		}
	}
}
