package pl.poznan.put.bwm.algorithm;

import java.util.Map;




public class ResultContainer {
	private Map<String, Double> weights;
	private boolean found_solution;
	private double ksi;
	private double consistencyRatio;
	
	public ResultContainer(Map<String, Double> weights, boolean found_solution, double ksi, double consistencyRatio) {
		super();
		this.weights = weights;
		this.found_solution = found_solution;
		this.ksi = ksi;
		this.consistencyRatio = consistencyRatio;
	}
	
	public Map<String, Double> getWeights() {
		return weights;
	}

	public boolean isFound_solution() {
		return found_solution;
	}

	public double getKsi() {
		return ksi;
	}

	public double getConsistencyRatio() {
		return consistencyRatio;
	}


	




	
	
}
