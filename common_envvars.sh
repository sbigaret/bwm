# Adapt the following lines
# NB: javafx.util.Pair is required: either install javafx or use Oracle Java 8
JAVA_HOME="${JAVA_HOME:-/usr/local/jdk1.8.0_181}"
# These 2 instruct the XMCDA Java lib to use a specific version when writing
export XMCDAv2_VERSION=2.2.3
export XMCDAv3_VERSION=3.1.1

# glpk_java
GLPK_JAVA_DYNLIB=./lib/libglpk_java.so

# -- You normally do not need to change anything beyond this point --
JARS="./lib/xmcda-0.6.jar:./lib/glpk-java.jar:target/BWM.jar"

if ! [ -e "${GLPK_JAVA_DYNLIB:?}" ]; then
    echo "libglpk_java dynamic lib. not found" >&2
    exit 1
fi

GLPK_JAVA_DYNLIB_DIR=${GLPK_JAVA_DYNLIB%/*}

JAVA="${JAVA_HOME}/bin/java"
export JAVA_HOME
CMD="${JAVA} -cp ${JARS} -Djava.library.path=${GLPK_JAVA_DYNLIB_DIR} pl.poznan.put.bwm.xmcda.WeightsXMCDA"
